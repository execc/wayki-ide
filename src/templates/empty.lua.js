const template = `
mylib = require "mylib"
--must start with mylib = require "mylib". Be sure to put it in the first line. If the first line is left blank, an exception will be reported.

--Define calling smart contract events
METHOD = {
    ENTRYPOINT  = 0x17
}

Entrypoint = function()
    --Do nothing. Define you implementation here
end

--Entry function of smart contract
Main = function()
  assert(#contract >=2, "Param length error (<2): " ..#contract )
  assert(contract[1] == 0xf0, "Param MagicNo error (~=0xf0): " .. contract[1])

  if contract[2] == METHOD.ENTRYPOINT then
  Entrypoint()
  else
    error('method# '..string.format("%02x", contract[2])..' not found')
  end
end

Main()
`.trim();

export default {
  name: 'New',
  discription: 'Minimally working contract with empty implementation',
  template,
};
