const template = `
mylib = require "mylib"
--must start with mylib = require "mylib". Be sure to put it in the first line. If the first line is left blank, an exception will be reported.

--Define calling smart contract events
METHOD = {
    INCREMENT  = 0x17
}

--Used to print log information to a file
LogMsg = function (msg)
   local logTable = {
        key = 0,
        length = string.len(msg),
        value = msg
  }
  mylib.LogPrint(logTable)
end

---------------------------------------------------

Increment = function()
    local key = "value"
    local storedData = {mylib.ReadData(key)};
    local initVal = 0;
    if (#storedData > 0) then
        initVal = mylib.ByteToInteger(Unpack(storedData));
    end

    local nextVal = initVal + 1;
    local nextValBytes = {mylib.IntegerToByte8(nextVal)}

    local writeDbTbl = {
        key = "value",
        length = #nextValBytes,
        value = nextValBytes
    }
    mylib.WriteData(writeDbTbl)
    LogMsg("Run CHECK_HELLOWORLD Method")
end

Unpack = function (t,i)
    i = i or 1
    if t[i] then
      return t[i], Unpack(t,i+1)
    end
end

--refer to[4.2Development Lua Methods]
--Entry function of smart contract
Main = function()
  assert(#contract >=2, "Param length error (<2): " ..#contract )
  assert(contract[1] == 0xf0, "Param MagicNo error (~=0xf0): " .. contract[1])

  if contract[2] == METHOD.INCREMENT then
    Increment()
  else
    error('method# '..string.format("%02x", contract[2])..' not found')
  end
end

Main()
`.trim();

export default {
  name: 'Counter',
  description: 'Simple increment-only counter',
  template,
};
