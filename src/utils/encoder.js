export function charToCode(e) {
  var t = '';
  e.split('').map(function(e) {
    t += e.charCodeAt().toString(16);
  });
  return t;
}

export function numberToCode(t, e) {
  if (typeof t === 'number') {
    t = t.toString();
  }
  var n = [],
    r = '',
    o = '';
  r = (r = (+t.slice()).toString(16)).length % 2 == 0 ? r : '0' + r;
  for (var i = 0; i < r.length; i += 2) n.push(r.substr(i, 2));
  o = n.reverse().join('');
  o += '00000000000000000000000000'.substr(0, e - o.length);
  return o;
}

export function strToutf8(e) {
  return getUTFCode(e);
}

export function getUTFCode(e) {
  for (var t = [], o = 0; o < e.length; o++) {
    var n = e.charCodeAt(o);
    0 <= n && n <= 127
      ? t.push(n)
      : 128 <= n && n <= 2047
      ? (t.push(192 | (31 & (n >> 6))), t.push(128 | (63 & n)))
      : ((2048 <= n && n <= 55295) || (57344 <= n && n <= 65535)) &&
        (t.push(224 | (15 & (n >> 12))), t.push(128 | (63 & (n >> 6))), t.push(128 | (63 & n)));
  }
  for (o = 0; o < t.length; o++) (t[o] &= 255), (t[o] = t[o].toString(16));
  return t.join('');
}

export function encodeCall(inputs) {
  var r = '';
  Object.keys(inputs).forEach(key => {
    let val = inputs[key].value;
    let type = inputs[key].type;
    r += TYPES[type].encoder(val);
  });
  return r;
}

export function decodeUTF8(v) {
  for (var t = [], e = 0; e < v.length; e += 2) t.push('%' + v.slice(e, e + 2));
  return decodeURIComponent(t.join(''));
}

export function decodeBase64(v) {
  return decodeURI(v);
}

export function reverseString(t) {
  return t
    .slice()
    .split('')
    .reverse()
    .join('');
}

export function codeToNumber(c) {
  for (var t = reverseString(c), e = 0, n = '', r = []; e < t.length; ) {
    if (0 != t[e]) {
      (n = t.substring(e)).length % 2 != 0 && (n = '0' + n), (n = reverseString(n));
      break;
    }
    e++;
  }
  for (var o = 0; o < n.length; o += 2) r.push(n.substr(o, 2));
  return parseInt(r.reverse().join(''), 16);
}

export function codeToChar(c) {
  for (var t = [], e = 0; e < c.length; e += 2) {
    var n = parseInt(c.slice(e, e + 2), 16);
    t.push(String.fromCharCode(n));
  }
  this.result = t.join('');
}

export function decodeCall(type, value) {
  return TYPES[type].decoder(value);
}

export const TYPES = {
  byte: {
    shortDesc: 'Byte',
    desc: 'Byte number (0-255) in HEX',
    encoder: n => ('' + n).replace('0x', ''),
    decoder: s => s,
  },
  short: {
    shortDesc: 'Short',
    desc: 'Short number (4 bytes)',
    encoder: n => numberToCode(n, 8),
    decoder: s => codeToNumber(s),
  },
  int: {
    shortDesc: 'Integer',
    desc: 'Long number (8 bytes)',
    encoder: n => numberToCode(n, 16),
    decoder: s => codeToNumber(s),
  },
  utf: {
    shortDesc: 'UTF',
    desc: 'String (UTF-8) type',
    encoder: n => strToutf8(n),
    decoder: s => decodeUTF8(s),
  },
};

/*
const result = decodeUTF8('6d796c6962203d2078202b202d202a202c207b207d202061626320d180d183d181d181d0bad0b8d0b9');
console.log(result);

const result2 = strToutf8('mylib = x + - * , { }  abc русский');
console.log(result2);

const result3 = decodeUTF8(result2);
console.log(result3);

const result4 = numberToCode(125, 8);
console.log(result4);

const result5 = codeToNumber(result4);
console.log(result5);
*/
