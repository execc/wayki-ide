export function auth() {
  const { Waves } = window;

  if (Waves) {
    return Waves.auth({
      name: 'Wayki IDE',
      data: 'Wayki IDE',
    }).then(authData => {
      return {
        name: authData.address,
        userid: authData.publicKey,
        signature: authData.publicKey,
        avatar: authData.address,
        group: 'authorized',
      };
    });
  }
  return Promise.resolve({});
}

export async function signUi(tx) {
  const { Waves } = window;

  const keeperTx = {
    type: tx.type,
    data: tx,
  };

  console.log(tx);

  const result = Waves.signTransaction(tx)
    .then(data => {
      console.log(data);
      return data;
    })
    .catch(err => {
      console.log(err);
    });
  console.log('!');
  return result;
}
