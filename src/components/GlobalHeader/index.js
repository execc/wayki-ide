import React, { PureComponent } from 'react';
import { Icon, Button, Input, Row, Col } from 'antd';
import Link from 'umi/link';
import Debounce from 'lodash-decorators/debounce';
import styles from './index.less';
import RightContent from './RightContent';

export default class GlobalHeader extends PureComponent {
  componentWillUnmount() {
    this.triggerResizeEvent.cancel();
  }
  /* eslint-disable*/
  @Debounce(600)
  triggerResizeEvent() {
    // eslint-disable-line
    const event = document.createEvent('HTMLEvents');
    event.initEvent('resize', true, false);
    window.dispatchEvent(event);
  }
  toggle = () => {
    const { collapsed, onCollapse } = this.props;
    onCollapse(!collapsed);
    this.triggerResizeEvent();
  };

  upload() {
    const f = document.getElementsByClassName('fileUpload')[0];
    if (f) {
      f.click();
    }
  }

  fileChanged = evt => {
    const { onUpload } = this.props;

    if (evt.target.files && evt.target.files[0]) {
      const file = evt.target.files[0];

      onUpload(file);
    }
  };

  render() {
    const { collapsed, isMobile, logo, onDownload, currentUser } = this.props;
    const style = {
      marginRight: 5,
    };

    const wiccBalance = currentUser.Balance
      ? currentUser.Balance / 100000000 + ' WICC'
      : '0git WICC';

    return (
      <div className={styles.header} style={{ marginBottom: 16 }}>
        {isMobile && (
          <Link to="/" className={styles.logo} key="logo">
            <img src={logo} alt="logo" width="32" />
          </Link>
        )}
        <span className={styles.trigger} onClick={this.toggle}>
          <Icon type={collapsed ? 'menu-unfold' : 'menu-fold'} />
        </span>

        <Button
          type="primary"
          shape="circle"
          icon="download"
          style={style}
          title="Download LUA script"
          onClick={onDownload}
        />
        <Button
          type="primary"
          shape="circle"
          icon="upload"
          style={style}
          title="Load LUA script"
          onClick={this.upload}
        />

        <input
          type="file"
          className="fileUpload"
          name="fileUpload"
          id="fileUpload"
          style={{ display: 'none' }}
          onChange={this.fileChanged}
        />

        <RightContent {...this.props} />

        <Input.Group>
          <Row gutter={15}>
            <Col span={8}>
              <Input
                addonBefore="Network"
                defaultValue={currentUser.network}
                disabled={true}
                value={currentUser.network}
              />
            </Col>
            <Col span={10}>
              <Input
                addonBefore="Address"
                defaultValue={currentUser.address}
                disabled={true}
                value={currentUser.address}
              />
            </Col>
            <Col span={4}>
              <Input
                addonBefore="Balance"
                defaultValue={wiccBalance}
                disabled={true}
                value={wiccBalance}
              />
            </Col>
          </Row>
        </Input.Group>
      </div>
    );
  }
}
