import React, { PureComponent } from 'react';
// import { formatMessage, setLocale, getLocale } from 'umi/locale';
import { Menu, Icon } from 'antd';
import classNames from 'classnames';
import HeaderDropdown from '../HeaderDropdown';
import styles from './index.less';

export default class Feedback extends PureComponent {
  changeFeedback = ({ key }) => {
    // setLocale(key);
    window.open(key);
  };

  render() {
    const { className } = this.props;

    const feedbacks = [
      {
        name: 'Telegram',
        href: 'https://t.me/easychaintech',
        icon: 'message',
      },
      {
        name: 'E-mail',
        href: 'mailto:all@easychain.tech&subject=IDE Feedback/Question',
        icon: 'mail',
      },
    ];

    const fedbackMenu = (
      <Menu className={styles.menu}>
        {feedbacks.map(feedback => (
          <Menu.Item key={feedback.name}>
            <a target="_blank" rel="noopener noreferrer" href={feedback.href}>
              <span role="img" aria-label={feedback.name}>
                <Icon type={feedback.icon} />
              </span>{' '}
              {feedback.name}
            </a>
          </Menu.Item>
        ))}
      </Menu>
    );
    return (
      <HeaderDropdown overlay={fedbackMenu} placement="bottomRight">
        <a
          target="_blank"
          rel="noopener noreferrer"
          href={feedbacks[0].href}
          className={classNames(styles.dropDown, className)}
        >
          <Icon type="notification" />
          <span>&nbsp;&nbsp;</span>
          Feedback
        </a>
      </HeaderDropdown>
    );
  }
}
