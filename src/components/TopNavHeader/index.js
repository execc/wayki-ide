import React, { PureComponent } from 'react';
import Link from 'umi/link';
import { Tag } from 'antd';
import RightContent from '../GlobalHeader/RightContent';
import Toolbar from '../Toolbar';
import styles from './index.less';

export default class TopNavHeader extends PureComponent {
  state = {
    maxWidth: undefined,
  };

  static getDerivedStateFromProps(props) {
    return {
      maxWidth: (props.contentWidth === 'Fixed' ? 1200 : window.innerWidth) - 280 - 165 - 40,
    };
  }

  render() {
    const { theme, contentWidth, logo, address, network, code } = this.props;
    let isSafe = true;
    if (code) {
      const { code: codeSrc } = code;
      isSafe = codeSrc === '' || codeSrc === undefined || codeSrc === null;
    }
    const { maxWidth } = this.state;
    return (
      <div className={`${styles.head} ${theme === 'light' ? styles.light : ''}`}>
        <div
          ref={ref => {
            this.maim = ref;
          }}
          className={`${styles.main} ${contentWidth === 'Fixed' ? styles.wide : ''}`}
        >
          <div className={styles.left}>
            <div className={styles.logo} key="logo" id="logo">
              <Link to="/">
                <img src={logo} alt="logo" />
                <h1>Wayki IDE</h1>
              </Link>
            </div>
            <div
              style={{
                maxWidth,
              }}
            >
              <Toolbar isSafe={isSafe} {...this.props} />
            </div>
          </div>

          {address && (
            <div>
              <Tag>{address}</Tag>
              <span>@</span>
              <Tag color={network === 'testnet' ? 'green' : 'red'}>{network}</Tag>
            </div>
          )}
          <RightContent {...this.props} />
        </div>
      </div>
    );
  }
}
