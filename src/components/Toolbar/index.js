import React, { PureComponent } from 'react';
import { Button, Divider, Dropdown, Menu, Icon, Modal } from 'antd';
import templates from '@/templates/templates';

export default class Toolbar extends PureComponent {
  confirm = (onOk, isSafe) => () => {
    if (isSafe) {
      onOk();
    } else {
      Modal.confirm({
        title: 'Code overwrite',
        content: 'Loading contract will overwrite you current code. Confirm overwriting?',
        cancelText: 'Cancel',
        okText: 'Ok',
        onOk,
        onCancel() {},
      });
    }
  };

  upload = () => {
    const f = document.getElementsByClassName('fileUpload')[0];
    if (f) {
      f.click();
    }
  };

  fileChanged = evt => {
    const { onUpload } = this.props;

    if (evt.target.files && evt.target.files[0]) {
      const file = evt.target.files[0];

      onUpload(file);
    }
  };

  handleTemplate = (name, code) => {
    const { onTemplate } = this.props;

    onTemplate(name, code);
  };

  render() {
    const { onDownload, isSafe } = this.props;
    const style = {
      marginRight: 5,
    };

    const menu = (
      <Menu>
        {Object.keys(templates).map(key => {
          const { name, description, template } = templates[key];

          return (
            <Menu.Item
              key={key}
              title={description}
              onClick={this.confirm(() => this.handleTemplate(name, template), isSafe)}
            >
              <Icon type="file-text" />
              {name}
            </Menu.Item>
          );
        })}
      </Menu>
    );

    return (
      <div>
        <Dropdown overlay={menu}>
          <Button
            type="primary"
            shape="circle"
            icon="file-add"
            style={style}
            title="Create new LUA script"
          />
        </Dropdown>
        <Divider type="vertical" />
        <Button
          type="primary"
          shape="circle"
          icon="download"
          style={style}
          title="Export LUA script"
          onClick={onDownload}
        />
        <Button
          type="primary"
          shape="circle"
          icon="upload"
          style={style}
          title="Import LUA script"
          onClick={this.confirm(this.upload, isSafe)}
        />

        <input
          type="file"
          className="fileUpload"
          name="fileUpload"
          id="fileUpload"
          style={{ display: 'none' }}
          onChange={this.fileChanged}
        />
      </div>
    );
  }
}
