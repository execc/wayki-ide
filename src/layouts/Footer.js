import React, { Fragment } from 'react';
import { Layout, Icon } from 'antd';
import GlobalFooter from '@/components/GlobalFooter';

const { Footer } = Layout;
const FooterView = () => (
  <Footer style={{ padding: 0 }}>
    <GlobalFooter
      links={[
        {
          key: 'Wayki IDE',
          title: 'Wayki IDE',
          href: '',
          blankTarget: true,
        },
      ]}
      contacts={
        <Fragment>
          Contact us <Icon type="mail" /> all@easychain.tech <Icon type="message" /> telegram: @easychaintech
        </Fragment>
      }
      copyright={
        <Fragment>
          Copyright <Icon type="copyright" /> 2019
        </Fragment>
      }
    />
  </Footer>
);
export default FooterView;
