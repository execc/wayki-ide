import React from 'react';
import { Card, Button, Divider, Modal, Input, Form, Empty, Alert, Popconfirm } from 'antd';
import styles from './Analysis.less';
import Caller from './Caller';
import { hasWicc } from '@/services/contract';

class Deployer extends React.PureComponent {
  state = {
    visible: false,
    regId: '',
  };

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleOk = () => {
    const { onContractLoaded } = this.props;
    const { regId } = this.state;
    this.setState({
      visible: false,
    });

    onContractLoaded(regId);
  };

  handleCancel = () => {
    this.setState({
      visible: false,
    });
  };

  handleValueChanged = e => {
    this.setState({
      regId: e.target.value,
    });
  };

  render() {
    const { loading, regIdLoading, onDeploy, isSafe, data, regId } = this.props;
    const { regId: modalRegId, visible } = this.state;

    const canInteractWithBlockchain = hasWicc();

    const buttons = (
      <div className={styles.salesCardExtra}>
        <div className={styles.salesTypeRadio}>
          <Button
            style={{ marginRight: 5 }}
            loading={loading}
            type="primary"
            icon="cloud-upload"
            title="Upload contract to Blockchain"
            onClick={onDeploy}
          >
            Publish
          </Button>

          {!isSafe ? (
            <Popconfirm
              placement="topLeft"
              title="Loading contract will overwrite you current code. Confirm overwriting?"
              onConfirm={this.showModal}
              okText="Yes"
              cancelText="No"
            >
              <Button
                loading={regIdLoading}
                type="default"
                icon="cloud-download"
                title="Load contract by RegId"
              >
                Load
              </Button>
            </Popconfirm>
          ) : (
            <Button
              loading={regIdLoading}
              type="default"
              icon="cloud-download"
              title="Load contract by RegId"
              onClick={this.showModal}
            >
              Load
            </Button>
          )}

          <Modal
            title="Enter RegId"
            visible={visible}
            cancelText="Cancel"
            okText="Ok"
            onOk={this.handleOk}
            onCancel={this.handleCancel}
          >
            <Form>
              <Form.Item label="Reg ID">
                <Input value={modalRegId} onChange={this.handleValueChanged} />
              </Form.Item>
            </Form>
          </Modal>
        </div>
      </div>
    );

    const info = (
      <div>
        <h3>Information</h3>
        <span>
          <b>RegId: </b>
          {regId}
        </span>
        <Divider />
        <Caller data={data} />
      </div>
    );

    const empty = (
      <Empty
        description={
          <Alert
            message="WICC Web Wallet is not installed"
            description={
              <span>
                Unable to interact with Blockchain.
                <br />
                Install{' '}
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://www.wiccdev.org/book/en/DeveloperHelper/webextension.html"
                >
                  Chrome extension
                </a>{' '}
                to deploy and call smart contracts and get{' '}
                <a
                  target="_blank"
                  rel="noopener noreferrer"
                  href="https://www.wiccdev.org/book/en/DeveloperHelper/developertool.html#get-wicc-testnet-tools"
                >
                Test coins
                </a>{' '}
              </span>
            }
            type="error"
          />
        }
      />
    );

    const renderError = !canInteractWithBlockchain;
    const renderInfo = !renderError && regId;

    return (
      <Card
        loading={false}
        className={styles.salesCard}
        bordered={false}
        title="Contract"
        bodyStyle={{ padding: 24 }}
        extra={canInteractWithBlockchain ? buttons : ''}
        style={{ marginTop: 0 }}
      >
        {renderError ? empty : ''}
        {renderInfo ? info : ''}
      </Card>
    );
  }
}

export default Deployer;
