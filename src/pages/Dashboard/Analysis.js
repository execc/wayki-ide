import React, { Component, Suspense } from 'react';
import { connect } from 'dva';
import { Row, Col } from 'antd';

import GridContent from '@/components/PageHeaderWrapper/GridContent';

import styles from './Analysis.less';

const Editor = React.lazy(() => import('./Editor'));
const Deployer = React.lazy(() => import('./Deployer'));

@connect(({ code, loading }) => ({
  code,
  loading: loading.effects['code/deployCode'],
  regIdLoading: loading.effects['code/changeRegId'],
}))
class Analysis extends Component {
  state = {};

  componentDidMount() {
    const { dispatch } = this.props;
    this.reqRef = requestAnimationFrame(() => {
      dispatch({
        type: 'code/fetch',
      });
    });
  }

  componentWillUnmount() {
    const { dispatch } = this.props;
    dispatch({
      type: 'code/clear',
    });
    cancelAnimationFrame(this.reqRef);
    clearTimeout(this.timeoutId);
  }

  changeCode = value => {
    const { dispatch } = this.props;

    dispatch({
      type: 'code/changeCode',
      payload: value,
    });
  };

  changeName = value => {
    const { dispatch } = this.props;

    dispatch({
      type: 'code/changeName',
      payload: value,
    });
  };

  handleDeploy = () => {
    const { dispatch, code } = this.props;
    const { code: codeSrc, name } = code;

    dispatch({
      type: 'code/deployCode',
      payload: {
        code: codeSrc,
        name,
      },
    });
  };

  onContractLoaded = regId => {
    const { dispatch } = this.props;

    dispatch({
      type: 'code/changeRegId',
      payload: regId,
    });
  };

  render() {
    const { loading, regIdLoading, code } = this.props;
    const { code: codeSrc, name, contract, call } = code;
    const { regId } = contract;
    const codeEmpty = codeSrc === null || codeSrc === '' || codeSrc === undefined;

    return (
      <GridContent>
        <div className={styles.twoColLayout}>
          <Row gutter={24}>
            <Col xl={18} lg={36} md={36} sm={36} xs={36}>
              <Suspense fallback={null}>
                <Editor
                  value={codeSrc}
                  onCodeChange={this.changeCode}
                  onNameChange={this.changeName}
                  name={name}
                />
              </Suspense>
            </Col>
            <Col xl={6} lg={12} md={12} sm={12} xs={12}>
              <Suspense fallback={null}>
                <Deployer
                  regId={regId}
                  data={call}
                  onDeploy={this.handleDeploy}
                  loading={loading}
                  regIdLoading={regIdLoading}
                  onContractLoaded={this.onContractLoaded}
                  isSafe={codeEmpty}
                />
              </Suspense>
            </Col>
          </Row>
        </div>
      </GridContent>
    );
  }
}

export default Analysis;
