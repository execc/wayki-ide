import React from 'react';
import { Card, Input } from 'antd';
import MonacoEditor from 'react-monaco-editor';

const options = {
  automaticLayout: true,
  selectOnLineNumbers: true,
};

class Editor extends React.Component {
  state = {
    editTitle: false,
  };

  toggleEdit = () => {
    this.setState(prevState => ({
      editTitle: !prevState.editTitle,
    }));
  };

  render() {
    const { editTitle } = this.state;
    const { name, onNameChange, onCodeChange, value } = this.props;

    return (
      <Card
        loading={false}
        bordered={false}
        title={
          editTitle ? (
            <Input
              autoFocus
              onBlur={() => {
                this.toggleEdit();
              }}
              value={name}
              onChange={x => onNameChange(x.target.value)}
            />
          ) : (
            `${name}.lua`
          )
        }
        extra={
          <a onClick={this.toggleEdit} href="#">
            Rename
          </a>
        }
        style={{ marginTop: 0 }}
      >
        <MonacoEditor
          language="lua"
          theme="vs"
          height={400}
          value={value}
          options={options}
          onChange={onCodeChange}
        />
      </Card>
    );
  }
}

export default Editor;
