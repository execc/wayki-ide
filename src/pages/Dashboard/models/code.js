import { notification } from 'antd';
import {
  deploy,
  callContract,
  getContractRegId,
  waitTx,
  getContractValue,
  getContractInfo,
} from '@/services/contract';
import { encodeCall, getUTFCode, decodeCall, decodeUTF8 } from '@/utils/encoder';

const debounce = (func, wait, immediate) => {
  let timeout;
  function result() {
    const context = this;
    const args = arguments; // eslint-disable-line prefer-rest-params
    const later = () => {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    const callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  }
  return result;
};

const persist = debounce(result => {
  localStorage.setItem('code', JSON.stringify(result));
}, 250);

export default {
  namespace: 'code',

  state: {
    code: '',
    name: 'example',
    contract: {
      regId: '',
    },
    callParams: {
      value: 0,
      inputs: {
        '0': {
          type: 'byte',
          value: 'f0',
        },
        '1': {
          type: 'byte',
          value: '17',
        },
      },
      keys: {
        value: {
          type: 'int',
        },
      },
    },
    callResult: {},
    loading: false,
  },

  effects: {
    *fetch(_, { put }) {
      const stored = localStorage.getItem('code');
      if (stored) {
        yield put({
          type: 'save',
          payload: JSON.parse(stored),
        });
      }
    },
    *changeName(payload, { put }) {
      yield put({
        type: 'save',
        payload: {
          name: payload.payload,
        },
      });
    },
    *changeCode(payload, { put }) {
      yield put({
        type: 'save',
        payload: {
          code: payload.payload,
        },
      });
    },
    *resetCode(payload, { put }) {
      yield put({
        type: 'save',
        payload: {
          code: payload.payload,
          contract: {
            regId: '',
          },
          callResult: {},
        },
      });
    },
    *changeRegId(payload, { call, put }) {
      const regId = payload.payload.trim();

      const response = yield call(getContractInfo, regId);
      if (response.result) {
        yield put({
          type: 'save',
          payload: {
            contract: {
              regId,
            },
          },
        });

        const content = response.result.contract_content;
        const code = decodeUTF8(content);

        yield put({
          type: 'save',
          payload: {
            code,
          },
        });
      } else {
        notification.error({
          message: `Contract with RegID ${regId} does not exist`,
          description: JSON.stringify(response.error),
        });
      }
    },
    *changeWatchedValues(payload, { put }) {
      yield put({
        type: 'save',
        payload: {
          contractData: payload.payload,
        },
      });
    },
    *saveCall(payload, { put }) {
      yield put({
        type: 'save',
        payload: {
          callParams: payload.payload,
        },
      });
    },
    *changeCall(payload, { call, put }) {
      const callParams = payload.payload;

      yield put({
        type: 'save',
        payload: {
          callParams,
        },
      });

      const { regId } = payload;
      const { inputs, keys, value } = callParams;

      const callStr = inputs ? encodeCall(inputs) : '';
      const response = yield call(callContract, regId, callStr, value);

      if (response.txid) {
        yield call(waitTx, response.txid);

        const callResult = {};
        if (keys) {
          // eslint-disable-next-line no-restricted-syntax
          for (const key in keys) {
            // eslint-disable-next-line no-prototype-builtins
            if (keys.hasOwnProperty(key)) {
              const { name, type } = keys[key];
              const keyRs = yield call(getContractValue, regId, getUTFCode(name));

              if (keyRs.result) {
                callResult[name] = {
                  ...keyRs.result,
                  value: decodeCall(type, keyRs.result.value),
                };
              }
              if (keyRs.error) {
                callResult[name] = {
                  ...keyRs.result,
                  value: keyRs.error.message,
                };
              }
            }
          }

          yield put({
            type: 'save',
            payload: {
              callResult,
            },
          });
        }
      }
    },
    *updateKeys(payload, { call, put }) {
      const callParams = payload.payload;

      yield put({
        type: 'save',
        payload: {
          callParams,
        },
      });

      const { regId } = payload;
      const { keys } = callParams;

      const callResult = {};
      if (keys) {
        // eslint-disable-next-line no-restricted-syntax
        for (const key in keys) {
          // eslint-disable-next-line no-prototype-builtins
          if (keys.hasOwnProperty(key)) {
            const { name, type } = keys[key];
            const keyRs = yield call(getContractValue, regId, getUTFCode(name));

            if (keyRs.result) {
              callResult[name] = {
                ...keyRs.result,
                value: decodeCall(type, keyRs.result.value),
              };
            }
            if (keyRs.error) {
              callResult[name] = {
                ...keyRs.result,
                value: keyRs.error.message,
              };
            }
          }
        }

        yield put({
          type: 'save',
          payload: {
            callResult,
          },
        });
      }
    },
    *deployCode(payload, { call, put }) {
      const { code, name } = payload.payload;

      const response = yield call(deploy, code, name);
      if (response.txid) {
        yield put({
          type: 'save',
          payload: {
            contract: {
              regId: '',
              callResult: {},
            },
          },
        });

        const deployResult = yield call(getContractRegId, response.txid);
        response.regId = deployResult.result.regid;

        yield put({
          type: 'save',
          payload: {
            contract: response,
          },
        });
      } else {
        const msg = JSON.stringify(response);
        notification.error({
          message: 'Unexpected error',
          description: msg,
        });
      }
    },
  },

  reducers: {
    save(state, { payload }) {
      const result = {
        ...state,
        ...payload,
      };
      persist(result);
      return result;
    },
  },
};
