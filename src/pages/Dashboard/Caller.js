import React from 'react';
import { connect } from 'dva';
import { Form, Input, Button, Row, Col, Icon, Select, Tooltip } from 'antd';
import { TYPES } from '@/utils/encoder';

@connect(({ code, loading }) => ({
  code,
  loading: loading.effects['code/changeCall'],
  updateLoading: loading.effects['code/updateKeys'],
}))
@Form.create()
class Caller extends React.PureComponent {
  renderKey(inputData) {
    const { key, name, type, value, onRemoveKey, getFieldDecorator } = inputData;

    return (
      <Form.Item key={`${key}-${name}`}>
        <Row align="middle">
          <Col span={21}>
            <Input.Group compact>
              {getFieldDecorator(`keys.${key}.name`, {
                initialValue: name,
                rules: [{ required: true, message: 'Key name is required' }],
              })(<Input style={{ width: '30%' }} placeholder="name" />)}
              {getFieldDecorator(`keys.${key}.type`, {
                initialValue: type,
                rules: [{ required: true, message: 'Type is required' }],
              })(this.renderTypeSelect())}
              <Tooltip placement="topLeft" title={value} arrowPointAtCenter>
                <Input style={{ width: '40%' }} placeholder="..." disabled value={value} />
              </Tooltip>
            </Input.Group>
          </Col>
          <Col span={3}>
            <Button
              style={{ marginLeft: 3 }}
              type="dashed"
              shape="circle"
              title="Remove this key"
              onClick={onRemoveKey}
            >
              <Icon type="minus-circle-o" size="small" theme="twoTone" twoToneColor="#eb2f96" />
            </Button>
          </Col>
        </Row>
      </Form.Item>
    );
  }

  renderTypeSelect = () => {
    return (
      <Select style={{ width: '30%' }}>
        {Object.keys(TYPES).map(key => (
          <Select.Option title={TYPES[key].desc} value={key} key={key}>
            {TYPES[key].shortDesc}
          </Select.Option>
        ))}
      </Select>
    );
  };

  renderInput(inputData) {
    const { index, type, value, getFieldDecorator, onRemoveInput } = inputData;

    return (
      <Form.Item key={index}>
        <Row align="middle" style={{ width: '100%' }}>
          <Col span={21}>
            <Input.Group compact>
              {getFieldDecorator(`inputs.${index}.type`, {
                initialValue: type,
                rules: [{ required: true, message: 'Type is required' }],
              })(this.renderTypeSelect())}
              {getFieldDecorator(`inputs.${index}.value`, {
                initialValue: value,
                rules: [{ required: true, message: 'Value is required' }],
              })(<Input style={{ width: '65%' }} placeholder="..." />)}
            </Input.Group>
          </Col>
          <Col span={3}>
            <Button
              style={{ marginLeft: 3 }}
              type="dashed"
              shape="circle"
              title="Remove this parameter"
              onClick={onRemoveInput}
            >
              <Icon type="minus-circle-o" size="small" theme="twoTone" twoToneColor="#eb2f96" />
            </Button>
          </Col>
        </Row>
      </Form.Item>
    );
  }

  render() {
    const { form, code, dispatch, loading, updateLoading } = this.props;
    const { regId } = code.contract;
    const { callParams, callResult } = code;
    const { getFieldDecorator, validateFields } = form;

    const onInvokeContract = () => {
      validateFields((err, values) => {
        if (!err) {
          dispatch({
            type: 'code/changeCall',
            payload: values,
            regId,
          });
        }
      });
    };

    const onUpdateKeys = () => {
      validateFields((err, values) => {
        if (!err) {
          dispatch({
            type: 'code/updateKeys',
            payload: values,
            regId,
          });
        }
      });
    };

    const onAddInput = () => {
      validateFields((_, values) => {
        const vals = {
          ...values,
        };

        if (!vals.inputs) {
          vals.inputs = {};
        }
        const { inputs } = vals;
        const next = `id${new Date().getTime()}`;
        inputs[next] = {
          type: 'byte',
          value: '0x00',
        };

        dispatch({
          type: 'code/saveCall',
          payload: vals,
        });
      });
    };

    const onRemoveInput = id => () => {
      validateFields((_, values) => {
        const vals = {
          ...values,
        };

        delete vals.inputs[id];

        dispatch({
          type: 'code/saveCall',
          payload: vals,
        });
      });
    };

    const onAddKey = () => {
      validateFields((_, values) => {
        const vals = {
          ...values,
        };

        if (!vals.keys) {
          vals.keys = {};
        }
        const { keys } = vals;
        const next = `key${Object.keys(keys).length + 1}`;
        keys[`key${new Date().getTime()}`] = {
          type: 'int',
          name: next,
        };

        dispatch({
          type: 'code/saveCall',
          payload: vals,
        });
      });
    };

    const onRemoveKey = id => () => {
      validateFields((_, values) => {
        const vals = {
          ...values,
        };
        delete vals.keys[id];

        dispatch({
          type: 'code/saveCall',
          payload: vals,
        });
      });
    };

    return (
      <Form layout="vertical" hideRequiredMark>
        <Form.Item>
          <h3>Invocation options</h3>
          <h4 style={{ marginTop: 0, marginBottom: 6 }}>Send coins to contract</h4>

          <Input.Group compact style={{ marginBottom: 5 }}>
            <Input style={{ width: '35%' }} defaultValue="Amount" disabled />

            {getFieldDecorator('value', {
              initialValue: callParams.value,
              rules: [
                {
                  pattern: /^(\d+)((?:\.\d+)?)$/,
                  message: 'Invalid format',
                },
              ],
            })(<Input style={{ width: '65%' }} />)}
          </Input.Group>
        </Form.Item>

        <h4 style={{ marginTop: 0, marginBottom: 6 }}>Input parameters</h4>

        {callParams.inputs &&
          Object.keys(callParams.inputs).map(index =>
            this.renderInput({
              index,
              getFieldDecorator,
              onRemoveInput: onRemoveInput(index),
              type: callParams.inputs[index].type,
              value: callParams.inputs[index].value,
            })
          )}

        <Form.Item>
          <Button type="dashed" onClick={onAddInput}>
            <Icon type="plus" /> Add parameter
          </Button>
        </Form.Item>

        <h4 style={{ marginTop: 0, marginBottom: 6 }}>Watched keys</h4>

        {callParams.keys &&
          Object.keys(callParams.keys).map(key => {
            const { name, type } = callParams.keys[key];
            return this.renderKey({
              key,
              name,
              type,
              getFieldDecorator,
              onRemoveKey: onRemoveKey(key),
              value: callResult[name] && callResult[name].value,
            });
          })}

        <Form.Item>
          <Button type="dashed" onClick={onAddKey}>
            <Icon type="plus" /> Add key
          </Button>
        </Form.Item>

        <Form.Item>
          <Button
            style={{ marginRight: 5 }}
            loading={loading}
            type="primary"
            onClick={onInvokeContract}
            icon="deployment-unit"
          >
            Invoke contract
          </Button>
          <Button loading={updateLoading} type="default" onClick={onUpdateKeys} icon="sync">
            Update keys
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

export default Caller;
