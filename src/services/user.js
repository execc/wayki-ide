import request from '@/utils/request';

export async function query() {
  return request('/api/users');
}

export async function queryCurrent() {
  return {};

  // return {
  //  name: 'Denis'
  // }

  // return request('/api/currentUser');
}
