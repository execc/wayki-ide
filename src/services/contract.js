import { Promise } from 'q';
import request from '@/utils/request';

const username = 'wiccuser';
const password = '123456';

export async function rpc(method, params) {
  const hash = window.btoa(`${username}:${password}`);
  const options = {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Basic ${hash}`,
    },
    method: 'POST',
    body: {
      jsonrpc: '2.0',
      id: new Date().getTime().toString(),
      method,
      params,
    },
  };

  return request('/wayki', options);
}

export function hasWicc() {
  const { WiccWallet } = window;

  return WiccWallet !== undefined && WiccWallet !== null;
}

export async function getUser() {
  const { WiccWallet } = window;

  return new Promise(resolve => {
    try {
      WiccWallet.getDefaultAccount().then(account => {
        resolve({
          ...account,
          success: true,
        });
      });
    } catch (e) {
      resolve({
        success: false,
        ...e,
      });
    }
  });
}

export async function getAccount(address) {
  return rpc('getaccountinfo', [address]);
}

export async function deploy(code, description) {
  const { WiccWallet } = window;

  return new Promise(resolve => {
    WiccWallet.publishContract(code, description, (error, data) => {
      resolve(data || error);
    });
  });
}

export async function getContractInfo(regId) {
  return rpc('getcontractinfo', [regId]);
}

export async function callContract(regId, callstr, value) {
  const { WiccWallet } = window;

  return new Promise(resolve => {
    WiccWallet.callContract(regId, callstr, value, (error, data) => {
      resolve(data || error);
    });
  });
}

export async function getContractRegId(tx) {
  return new Promise(resolve => {
    async function internal() {
      const result = await rpc('getcontractregid', [tx]);
      if (result.result) {
        resolve(result);
      } else {
        setTimeout(internal, 500);
      }
    }

    internal();
  });
}

export async function getContractValue(regId, key) {
  return rpc('getcontractdataraw', [regId, key]);
}

export async function waitTx(tx) {
  return new Promise(resolve => {
    async function internal() {
      const result = await rpc('gettxdetail', [tx]);
      if (!result.result) {
        setTimeout(internal, 500);
      } else {
        const { confirmHeight, confirmedheight } = result.result;
        if (confirmHeight || confirmedheight) {
          resolve(result);
        } else {
          setTimeout(internal, 500);
        }
      }
    }

    internal();
  });
}
