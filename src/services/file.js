import * as download from 'downloadjs';

export function toFs(code, name) {
  download(code, name + '.lua', 'application/x-lue');
}

export async function fromFs(file) {
  const temporaryFileReader = new FileReader();

  return new Promise((resolve, reject) => {
    temporaryFileReader.onerror = () => {
      temporaryFileReader.abort();
      reject(new DOMException('Problem parsing input file.'));
    };

    temporaryFileReader.onload = () => {
      resolve(temporaryFileReader.result);
    };
    temporaryFileReader.readAsText(file);
  });
}
