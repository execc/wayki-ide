export default [
  // app
  {
    path: '/',
    component: '../layouts/BasicLayout',
    Routes: ['src/pages/Authorized'],
    authority: ['admin', 'user'],
    routes: [
      // dashboard
      { path: '/', redirect: '/editor/code' },
      {
        path: '/editor',
        name: 'editor',
        icon: 'edit',
        routes: [
          {
            path: '/editor/code',
            name: 'code',
            component: './Dashboard/Analysis',
          },
        ],
      },
      {
        component: '404',
      },
    ],
  },
];
