# Wayki IDE


IDE for developing, deploying and debugging smart contracts for WaykiChain Blockchain


![Alt text](waiky-ide-screenshot.png?raw=true "Inerface of Wayki IDE")


- Server: https://wayki-ide.easychain.tech
- Presentation Video: [WaykiIDE-Video](https://drive.google.com/file/d/1a4h5ah7TdQ2kZ6HLxOh-zD89T9aX9EZA/view?usp=sharing)


## Preconditions to start using Wayki IDE


Download WaykiChain wallet extention, launch it and activate. 

Link to get an extention -  https://www.wiccdev.org/book/en/DeveloperHelper/webextension.html

Link to get testnet coins - https://www.wiccdev.org/book/en/DeveloperHelper/developertool.html#get-wicc-testnet-tools

## Features

- See your current account information (network, address, balance) from launched Wayki web extention. ([See precondition](##preconditions-to-start-using-wayki-ide))
- Lua syntax highlighting
- Write smart contract code yourself or create new from the templates.
- Import smart contract source code from the external local file.
- Load deployed smart contract to your IDE by id
- Save written code to your local storage
- Publish written smart contract to a current selected blockchain network.
- Invoke contract with input parameters or send coins to the contract.
- Watch key values from the invoked smart contract.

## Templates

[Simple_Counter_Example]( smart-contract-example/hello.lua)

## Usage

### Use bash

```bash
$ git clone git@bitbucket.org:execc/wayki-ide.git
$ cd wayki-ide
$ npm install
$ npm run start:no-mock         # visit http://localhost:8000
```

### Use Case Description
1. Launch IDE in browser using server link (https://wayki-ide.easychain.tech) or [use bash](###use-bash)
2. Login in Wayki Web extention. [See links and precondions](##preconditions-to-start-using-wayki-ide)
3. CREATE 
    - From template: new "Counter" smart contract
    - IMPORT from you computer (counter example [Simple_Counter_Example]( smart-contract-example/hello.lua))
    - Load from blockchain by smart contract ID (Counter id 85536-1)
5. PUBLISH: Click "Publish" and confirm it by Wayki Extention. Wait a litle bit while transation will be peformed.
6. DEBUG: Write data to the fileds without brackets and then Click "Invoke contract" to see the values of keys of the smart contract: 
    - Send coins to contract - "0"
    - Input parameter 
        - type "Byte" value "f0"
        - type "Byte" value "17"
    - Watched Keys name "value" type "Integer"
7. EXPORT: You can change the lua code and click export button from the top panel to save new version localy.



## Technology used
Monaco, React, Redux, UMI, Redux-Saga, Wayki Web Wallet

## Future work
- Language server for semantic analysis
- Templates and Examples
- Code generation based on State machine diagram
- Local Lua Environment support
- Add console